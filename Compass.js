import React from "react";
import styled from 'styled-components'
import Icon from 'react-native-vector-icons/FontAwesome';

const Arrow = styled(Icon)`
	margin: auto;
	transform: rotate(${props => props.rotation}rad) rotateX(45deg) ;
    color: black;
    font-size: 300px;
`;

export default (props) => (
    <Arrow name="chevron-circle-up" size={30} color="#900" rotation={props.rotation} />
);
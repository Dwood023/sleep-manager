import React, { Component } from "react";
import styled, {keyframes} from 'styled-components'
import { Text, View } from "react-native";
import Compass from './Compass';

const MainView = styled(View)`
	margin: auto;
	padding: 2px;
`;

export default class App extends Component {
	
	constructor(props) {
		super(props);

		this.state = {
			location: {
				longitude: 0,
				latitude: 0
			},
			watchID: null
		};
	}

	spoof_gps = {
		longitude: 51.441935, 
		latitude: -0.946121
	}

	success = gps_position => {
		console.log("Got GPS!");
		let own_loc = {
			location: {
				longitude: gps_position.coords.longitude,
				latitude: gps_position.coords.latitude
			}
		}
		this.setState({ location: own_loc });

		// post
		fetch('10.30.85.31:3000', {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(own_loc),
		});
	}

	// Where are we gonna get location from if indoors or something???
	fail = error => {
		console.log("Failed to get GPS!");
		this.setState({location: this.spoof_gps});
	}

	componentDidMount() {
		// Update when location updates
		let watchID = navigator.geolocation.watchPosition(
			this.success, 
			this.fail,
			{ 
				timeout: 100,
				enableHighAccuracy: false 
			}
		)

		// Get initial value
		navigator.geolocation.getCurrentPosition(
			this.success,
			this.fail,
			{
				timeout: 100,
				enableHighAccuracy: false
			}
		)

		this.setState({watchID});

	}
		
	componentWillUnmount() {
		navigator.geolocation.clearWatch(this.state.watchID)
	}

	render() {
		let other = {
			longitude: 52.123123,
			latitude: -0.801235
		}
		let difference = {
			longitude: this.state.location.longitude - other.longitude,
			latitude: this.state.location.latitude - other.latitude
		}
		let degrees = Math.tan(difference.longitude, difference.latitude);
		console.log(degrees);
		
		return (
			<MainView>
				<Text>INSANE PERSON DETECTED!!</Text>
				<Compass rotation={degrees}/>
			</MainView>
		);
	}
}
		